#ifndef NQS_FINDDIST_HH
#define NQS_FINDDIST_HH

#include <vector>
#include <set>
#include <queue>

namespace nqs{


std::vector<int> FindDist(const std::vector<std::vector<int> > & g,int root){
  int n=g.size();
  std::vector<int> dists(n,-1);

  dists[root]=0;

  std::queue<int> tovisit;

  tovisit.push(root);

  while(tovisit.size()>0){
    int node=tovisit.front();
    tovisit.pop();

    for(int j=0;j<g[node].size();j++){
      int nj=g[node][j];

      if(dists[nj]==-1){
        tovisit.push(nj);
        dists[nj]=dists[node]+1;
      }
    }
  }

  return dists;
}

}
#endif
