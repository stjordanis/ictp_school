#ifndef NQS_SAMPLER_HH
#define NQS_SAMPLER_HH

namespace nqs{
  template<class RbmState> class Gibbs;
  template<class RbmState> class GibbsPt;
  template<class RbmState> class Metropolis;
}

#include "gibbs.hh"
#include "gibbs_pt.hh"
#include "metropolis.hh"
#endif
