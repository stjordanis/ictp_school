import numpy as np
import matplotlib.pyplot as plt

plt.ion()


exact=-1.274549484318e+00*20


while(True):
    plt.clf()
    plt.ylabel('Energy')
    plt.xlabel('Iteration #')
    results=np.loadtxt('20_1.log')
    nres=len(results[:,1])
    plt.plot(np.arange(nres),results[:,1],color='red')
    plt.axhline(y=exact, xmin=0, xmax=results[-1:,0], linewidth=2, color = 'k',label='Exact')
    if(nres>400):
        fitx=results[-400:-1,0]
        fity=results[-400:-1,1]
        z=np.polyfit(fitx,fity,deg=0)
        p = np.poly1d(z)

        plt.plot(fitx,p(fitx))
        if(nres>2000):
            plt.xlim([nres-2000,nres])
            maxval=np.max(results[-2000:-1,1])
            plt.ylim([exact-(np.abs(exact)*0.01),maxval+np.abs(maxval)*0.01])
        error=(z[0]-exact)/-exact
        plt.gca().text(0.95, 0.8, 'Relative Error : '+"{:.2e}".format(error),
        verticalalignment='bottom', horizontalalignment='right',
        color='green', fontsize=15,transform=plt.gca().transAxes)


    plt.legend(frameon=False)
    plt.pause(0.05)



plt.show()
