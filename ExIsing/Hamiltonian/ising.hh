#ifndef NQS_ISING1D_HH
#define NQS_ISING1D_HH

#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <complex>
#include <vector>

namespace nqs{

using namespace std;
using namespace Eigen;

//Transverse Field Ising model on an arbitrary graph
template<class Graph> class Ising{

  const int nspins_;
  double J_;
  double h_;

  const Graph & graph_;

  //list of bonds for the interaction part
  std::vector<std::vector<int>> bonds_;

public:

  Ising(const Graph & graph,double h,double J=1):
       nspins_(graph.Nsites()),h_(h),J_(J),graph_(graph){

    GenerateBonds();
  }

  void GenerateBonds(){
    auto adj=graph_.AdjacencyList();

    bonds_.resize(nspins_);

    for(int i=0;i<nspins_;i++){
      for (auto s : adj[i]){
        if(s>i){
          bonds_[i].push_back(s);
        }
      }
    }
  }

  //Finds the connected elements of the Hamiltonians
  //i.e. all the X'(k) such that H(X,X'(k))\neq0
  //for this model we have k=0,1,...N_spins
  //where k=0 is the diagonal element X'(0)=X
  //input is:
  //X(i), a +1/-1 vector containing the state X
  //output is:
  //mel(k), matrix elements H(X,X'(k))
  //connector(k), for each k contains a list of spins that should be flipped to obtain X'(k)
  //starting from X
  void FindConn(const VectorXd & v,vector<double> & mel,vector<vector<int>> & connectors,vector<vector<double>> & newconfs){

    connectors.clear();
    connectors.resize(nspins_+1);
    mel.resize(nspins_+1);

    mel[0]=0;
    connectors[0].resize(0);

    for(int i=0;i<nspins_;i++){
      //spin flips
      mel[i+1]=-h_;
      connectors[i+1].push_back(i);

      //interaction part
      for(auto bond : bonds_[i]){
        mel[0]-=J_*v(i)*v(bond);
      }
    }

  }

  int Nspins()const{
    return nspins_;
  }

};


}

#endif
