#ifndef NQS_ADAMAX_HH
#define NQS_ADAMAX_HH

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <cassert>
#include <cmath>

namespace nqs{

using namespace std;
using namespace Eigen;

class AdaMax{

  int npar_;

  double alpha_;
  double beta1_;
  double beta2_;

  VectorXd ut_;
  VectorXd mt_;

  VectorXd mask_;

  double niter_;
  double niter_reset_;

public:

  AdaMax(double alpha=0.001,double beta1=0.9,double beta2=0.999):alpha_(alpha),beta1_(beta1),beta2_(beta2){
    npar_=-1;
    niter_=0;
    niter_reset_=-1;
  }

  void SetNpar(int npar){

    npar_=npar;
    ut_.setZero(npar);
    mt_.setZero(npar);

    if(mask_.size()!=npar){
      mask_.setOnes(npar_);
    }
  }

  void SetMask(const VectorXd & mask){
    mask_=mask;
  }

  void Update(const VectorXd & grad,VectorXd & pars){

    assert(npar_>0);

    mt_=beta1_*mt_+(1.-beta1_)*grad;

    for(int i=0;i<npar_;i++){
      ut_(i)=std::max(std::abs(grad(i)),beta2_*ut_(i));
    }
    niter_+=1.;
    if(niter_reset_>0){
      if(niter_>niter_reset_){
        niter_=1;
      }
    }

    double eta=alpha_/(1.-std::pow(beta1_,niter_));
    for(int i=0;i<npar_;i++){
      pars(i)-=eta*mt_(i)/ut_(i)*mask_(i);
    }

  }


  void Reset(){
    ut_=VectorXd::Zero(npar_);
    mt_=VectorXd::Zero(npar_);
    niter_=0;
  }

  void SetResetEvery(double niter_reset){
    niter_reset_=niter_reset;
  }
};


}

#endif
