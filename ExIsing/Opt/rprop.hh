#ifndef NQS_RPROP_HH
#define NQS_RPROP_HH

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <cassert>
#include <cmath>

namespace nqs{

using namespace std;
using namespace Eigen;

class Rprop{

  //decay constant
  double etap_;
  double etam_;

  int npar_;

  double decay_factor_;

  VectorXd oldgrad_;
  VectorXd delta_;

  double deltamin_;
  double deltamax_;
  double delta0_;

public:

  Rprop(double etam,double etap,double delta0,double deltamin,double deltamax):
    etap_(etap),etam_(etam),deltamin_(deltamin),deltamax_(deltamax),delta0_(delta0){
    npar_=-1;
  }

  void SetNpar(int npar){
    npar_=npar;

    oldgrad_.resize(npar_);
    oldgrad_=VectorXd::Ones(npar_);
    delta_.resize(npar_);
    delta_=delta0_*VectorXd::Ones(npar_);

  }

  void Update(const VectorXd & grad,VectorXd & pars){
    assert(npar_>0);
    double normgrad=1./std::sqrt(grad.norm());

    for(int i=0;i<npar_;i++){
      if(grad(i)*oldgrad_(i)>0){
        delta_(i)=std::min(deltamax_,delta_(i)*etap_);
        oldgrad_(i)=grad(i);
      }
      else if(grad(i)*oldgrad_(i)<0){
        delta_(i)=std::max(deltamin_,delta_(i)*etam_);
        oldgrad_(i)=0;
      }
      else{
        oldgrad_(i)=grad(i);
      }
      if(grad(i)>0){
        pars(i)-=std::min(delta_(i),normgrad);
      }
      else if(grad(i)<0){
        pars(i)+=std::min(delta_(i),normgrad);
      }
    }
    cerr<<delta_.mean()<<endl;
  }


  void Reset(){

  }
};


}

#endif
