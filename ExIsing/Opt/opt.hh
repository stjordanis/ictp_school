#ifndef NQS_OPT_HH
#define NQS_OPT_HH

namespace nqs{
  template<class Hamiltonian,class RbmState,class Sampler,class Optimizer> class Variational;
  class Sgd;
  class AdaDelta;
  class AdaMax;
  class Rprop;
}

#include "variational.hh"
#include "sgd.hh"
#include "ada_delta.hh"
#include "ada_max.hh"
#include "rprop.hh"

#endif
