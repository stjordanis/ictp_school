#ifndef NQS_ADADELTA_HH
#define NQS_ADADELTA_HH

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <cassert>
#include <cmath>

namespace nqs{

using namespace std;
using namespace Eigen;

class AdaDelta{

  int npar_;

  //decay constant
  double rho_;

  //small parameter
  double eps_;

  VectorXd Eg2_;
  VectorXd Edx2_;


public:

  AdaDelta(double rho=0.95,double eps=1.0e-6):rho_(rho),eps_(eps){
    npar_=-1;
  }

  void SetNpar(int npar){
    npar_=npar;
    Eg2_.setZero(npar);
    Edx2_.setZero(npar);


  }

  void Update(const VectorXd & grad,VectorXd & pars){
    assert(npar_>0);

    Eg2_=rho_*Eg2_+(1.-rho_)*grad.cwiseAbs2();

    VectorXd Dx(npar_);

    for(int i=0;i<npar_;i++){
      Dx(i)=-std::sqrt(Edx2_(i)+eps_)*grad(i);
      Dx(i)/=std::sqrt(Eg2_(i)+eps_);
      pars(i)+=Dx(i);
    }

    Edx2_=rho_*Edx2_+(1.-rho_)*Dx.cwiseAbs2();

  }


  void Reset(){
    Eg2_=VectorXd::Zero(npar_);
    Edx2_=VectorXd::Zero(npar_);
  }
};


}

#endif
